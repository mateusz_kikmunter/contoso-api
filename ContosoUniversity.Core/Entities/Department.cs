﻿using System;
using System.Collections.Generic;

namespace ContosoUniversity.Core.Entities
{
    public class Department
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public IList<Course> Courses { get; set; } = new List<Course>();
    }
}
