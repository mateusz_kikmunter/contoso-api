﻿using System.Collections.Generic;

namespace ContosoUniversity.Core.Entities
{
    public class Instructor : Person
    {
        public IList<Course> Courses { get; set; } = new List<Course>();

        public Department Department { get; set; }
    }
}
