﻿using System;
using System.Collections.Generic;
using ContosoUniversity.Core.Enums;

namespace ContosoUniversity.Core.Entities
{
    public class Course
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public CourseStatus Status { get; set; }

        public Instructor Instructor { get; set; }

        public Department Department { get; set; }

        public IList<Enrollment> Enrollments { get; set; } = new List<Enrollment>();
    }
}
