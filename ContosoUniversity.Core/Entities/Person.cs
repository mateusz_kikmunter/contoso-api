﻿using System;

namespace ContosoUniversity.Core.Entities
{
    public abstract class Person
    {
        public Guid Id { get; set; }

        public Guid AuthenticationId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public bool Active { get; set; }
    }
}
