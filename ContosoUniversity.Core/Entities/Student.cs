﻿using System.Collections.Generic;

namespace ContosoUniversity.Core.Entities
{
    public class Student : Person
    {
        public IList<Enrollment> Enrollments { get; set; } = new List<Enrollment>();
    }
}
