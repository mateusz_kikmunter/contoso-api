﻿using System;
using ContosoUniversity.Core.Enums;

namespace ContosoUniversity.Core.Entities
{
    public class Enrollment
    {
        public Guid Id { get; set; }

        public Grade? Grade { get; set; }

        public Course Course { get; set; }

        public Student Student { get; set; }
    }
}
