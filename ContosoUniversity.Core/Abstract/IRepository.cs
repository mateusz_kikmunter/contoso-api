﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ContosoUniversity.Core.Abstract
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        Task<List<T>> GetAllAsync();

        Task<T> GetSingleAsync(object id);

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);

        Task<List<T>> FindByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);

        Task<T> AddAsync(T entity);

        Task<int> DeleteAsync(T entity);

        Task<int> UpdateAsync(T entity);
    }
}
