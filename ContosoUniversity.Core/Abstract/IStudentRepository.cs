﻿using ContosoUniversity.Core.Entities;

namespace ContosoUniversity.Core.Abstract
{
    public interface IStudentRepository : IRepository<Student>
    {
    }
}
