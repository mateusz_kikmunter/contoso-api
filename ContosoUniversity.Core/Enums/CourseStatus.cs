﻿namespace ContosoUniversity.Core.Enums
{
    public enum CourseStatus
    {
        Active,
        Finished
    }
}
