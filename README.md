# About
Old EF Core and MVC tutorial rewritten to .NET Core WebApi + JWT authentication.

https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/?view=aspnetcore-2.0

This is web api part.

Angular spa - https://bitbucket.org/mateusz_kikmunter/contoso-client/src/master/

Identity Server part - https://bitbucket.org/mateusz_kikmunter/contoso-api-auth-server/src/master/

# Front-end
Front-end part of the app will be written in Angular 6.