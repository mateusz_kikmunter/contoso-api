﻿using ContosoUniversity.Infrastructure.DataAccess.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.Infrastructure.Helpers
{
    public class ApplicationDbContextFactory : DesignTimeDbContextFactoryBase<ApplicationDbContext>
    {
        protected override ApplicationDbContext CreateNewInstance(DbContextOptions<ApplicationDbContext> options)
        {
            return new ApplicationDbContext(options);
        }
    }
}
