﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Infrastructure.Migrations
{
    public partial class Index_On_AuthenticationId_For_Student_And_Instructor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Students_AuthenticationId",
                table: "Students",
                column: "AuthenticationId");

            migrationBuilder.CreateIndex(
                name: "IX_Instructors_AuthenticationId",
                table: "Instructors",
                column: "AuthenticationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Students_AuthenticationId",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Instructors_AuthenticationId",
                table: "Instructors");
        }
    }
}
