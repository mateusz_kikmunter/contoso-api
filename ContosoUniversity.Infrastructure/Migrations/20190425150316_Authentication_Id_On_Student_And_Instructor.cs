﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Infrastructure.Migrations
{
    public partial class Authentication_Id_On_Student_And_Instructor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AuthenticationId",
                table: "Students",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "AuthenticationId",
                table: "Instructors",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthenticationId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "AuthenticationId",
                table: "Instructors");
        }
    }
}
