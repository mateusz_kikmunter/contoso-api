﻿using ContosoUniversity.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContosoUniversity.Infrastructure.DataAccess.Config
{
    public class InstructorConfiguration : IEntityTypeConfiguration<Instructor>
    {
        public void Configure(EntityTypeBuilder<Instructor> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Id).ValueGeneratedOnAdd();
            builder.Property(i => i.AuthenticationId);
            builder.HasIndex(i => i.AuthenticationId);
            builder.Property(i => i.Email).IsRequired().HasMaxLength(255);
            builder.Property(i => i.FirstName).IsRequired().HasMaxLength(255);
            builder.Property(i => i.LastName).IsRequired().HasMaxLength(255);
            builder.HasOne(i => i.Department);
            builder.HasMany(i => i.Courses).WithOne(c => c.Instructor);
        }
    }
}
