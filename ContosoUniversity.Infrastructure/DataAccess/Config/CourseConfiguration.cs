﻿using ContosoUniversity.Core.Entities;
using ContosoUniversity.Core.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContosoUniversity.Infrastructure.DataAccess.Config
{
    public class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd();
            builder.Property(c => c.Status).IsRequired().HasDefaultValue(CourseStatus.Active);
            builder.Property(c => c.Title).IsRequired().HasMaxLength(255);
            builder.HasOne(c => c.Instructor).WithMany(i => i.Courses);
            builder.HasOne(c => c.Department).WithMany(i => i.Courses);
            builder.HasMany(c => c.Enrollments);
        }
    }
}
