﻿using ContosoUniversity.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContosoUniversity.Infrastructure.DataAccess.Config
{
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.HasKey(s => s.Id);
            builder.Property(s => s.Id).ValueGeneratedOnAdd();
            builder.Property(s => s.AuthenticationId);
            builder.HasIndex(s => s.AuthenticationId);
            builder.Property(s => s.Email).IsRequired().HasMaxLength(255);
            builder.Property(s => s.FirstName).IsRequired().HasMaxLength(255);
            builder.Property(s => s.LastName).IsRequired().HasMaxLength(255);
            builder.HasMany(s => s.Enrollments).WithOne(e => e.Student);
        }
    }
}
