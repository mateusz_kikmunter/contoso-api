﻿using ContosoUniversity.Core.Abstract;
using ContosoUniversity.Core.Entities;
using ContosoUniversity.Infrastructure.DataAccess.DbContexts;

namespace ContosoUniversity.Infrastructure.DataAccess.Repositories
{
    public class StudentRepository : RepositoryBase<Student>, IStudentRepository
    {
        public StudentRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
