﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ContosoUniversity.Core.Abstract;
using ContosoUniversity.Infrastructure.DataAccess.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.Infrastructure.DataAccess.Repositories
{
    public class RepositoryBase<T> : IRepository<T> where T : class 
    {
        protected readonly ApplicationDbContext _context;

        protected RepositoryBase(ApplicationDbContext context)
        {
            _context = context;
        }

        public virtual IQueryable<T> GetAll()
        {
            return Set();
        }

        public virtual async Task<List<T>> GetAllAsync()
        {
            return await Set().ToListAsync();
        }

        public virtual async Task<T> GetSingleAsync(object id)
        {
            return await Set().FindAsync(id);
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            var query = Set().Where(predicate);
            return includes.Aggregate(query, (current, includeKey) => current.Include(includeKey));
        }

        public virtual async Task<List<T>> FindByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            var query = Set().Where(predicate);
            return await includes.Aggregate(query, (current, includeKey) => current.Include(includeKey)).ToListAsync();
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            await Set().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<int> DeleteAsync(T entity)
        {
            Set().Remove(entity);
            return await _context.SaveChangesAsync();
        }

        public virtual async Task<int> UpdateAsync(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        private DbSet<T> Set()
        {
            return _context.Set<T>();
        }
    }
}
