﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversity.Core.Entities;
using ContosoUniversity.Infrastructure.DataAccess.Repositories;
using ContosoUniversity.UnitTests.Helpers;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace ContosoUniversity.UnitTests.RepositoriesTests
{
    public class StudentRepositoryTests
    {
        [Fact]
        public async Task GetAllAsync_Shoud_Return_All_Students()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);

                //act
                var result = await repository.GetAllAsync();
            
                //assert
                result.Count.Should().Be(GetStudentsForTesting().Count);

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task GetSingleAsync_Should_Return_Student()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);
                var studentToFind = await context.Students.FirstAsync();

                //act

                var result = await repository.GetSingleAsync(studentToFind.Id);

                //assert
                result.Should().BeEquivalentTo(studentToFind);

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task GetSingleAsync_Should_Return_Null_When_Student_Does_not_Exist()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);

                //act

                var result = await repository.GetSingleAsync(Guid.Empty);

                //assert
                result.Should().BeNull();

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task DeleteAsync_Should_Remove_Student()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);

                //act
                var studentToDelete = await context.Students.FirstAsync();
                var totalStudents = await context.Students.CountAsync();
                await repository.DeleteAsync(studentToDelete);

                //assert
                context.Students.Count().Should().Be(totalStudents - 1);

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task AddAsync_Should_Create_Student()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);
                //act
                var student = new Student
                {
                    AuthenticationId = Guid.NewGuid(),
                    Active = true,
                    Enrollments = new List<Enrollment>(),
                    Email = "new.student@contoso.edu.com",
                    FirstName = "new",
                    LastName = "student"
                };

                var totalCount = await context.Students.CountAsync();
                var result = await repository.AddAsync(student);

                //assert
                result.Should().NotBeNull();
                result.Id.Should().NotBe(Guid.Empty);
                context.Students.Count().Should().Be(totalCount + 1);

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task UpdateAsync_Should_Update_Student()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);

                //act
                var student = await context.Students.FirstAsync();
                student.FirstName = "Bruce";
                student.LastName = "Lee";

                await repository.UpdateAsync(student);

                //assert
                var updatedStudent = await context.Students.FirstAsync();
                updatedStudent.FirstName.Should().Be(student.FirstName);
                updatedStudent.LastName.Should().Be(student.LastName);

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task FindByAsync_StudentExists_Should_Return_student()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);

                //act
                var result = await repository.FindByAsync(student => student.FirstName.Equals("Bruce"));

                //assert
                result.Should().NotBeNull();
                result.Count.Should().Be(1);

                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task FindByAsync_StudentDoesNotExist_Should_Return_EmptyCollection()
        {
            using (var context = await InMemoryDbContextFactory.CreateContext().SeedAsync(GetStudentsForTesting()))
            {
                //arrange
                var repository = new StudentRepository(context);

                //act
                var result = await repository.FindByAsync(student => student.FirstName.Equals("Bane"));

                //assert
                result.Should().BeNullOrEmpty();

                context.Database.EnsureDeleted();
            }
        }

        private IList<Student> GetStudentsForTesting()
        {
            return new List<Student>
            {
                new Student
                {
                    Id = Guid.NewGuid(),
                    AuthenticationId = Guid.NewGuid(),
                    Active = true,
                    Enrollments = new List<Enrollment>(),
                    Email = "Matt.Kick@contoso.edu.com",
                    FirstName = "Matt",
                    LastName = "Kick"
                },

                new Student
                {
                    Id = Guid.NewGuid(),
                    AuthenticationId = Guid.NewGuid(),
                    Active = true,
                    Enrollments = new List<Enrollment>(),
                    Email = "Bruce.Wayne@contoso.edu.com",
                    FirstName = "Bruce",
                    LastName = "Wayne"
                },

                new Student
                {
                    Id = Guid.NewGuid(),
                    AuthenticationId = Guid.NewGuid(),
                    Active = false,
                    Enrollments = new List<Enrollment>(),
                    Email = "Tony.Stark@contoso.edu.com",
                    FirstName = "Tony",
                    LastName = "Stark"
                },

                new Student
                {
                    Id = Guid.NewGuid(),
                    AuthenticationId = Guid.NewGuid(),
                    Active = false,
                    Enrollments = new List<Enrollment>(),
                    Email = "Natasha.Romanova@contoso.edu.com",
                    FirstName = "Natasha",
                    LastName = "Romanova"
                }
            };
        }
    }
}
