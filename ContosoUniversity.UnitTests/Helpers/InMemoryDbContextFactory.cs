﻿using ContosoUniversity.Infrastructure.DataAccess.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.UnitTests.Helpers
{
    public static class InMemoryDbContextFactory
    {
        public static ApplicationDbContext CreateContext() => CreateInMemoryDbContext();

        private static ApplicationDbContext CreateInMemoryDbContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlite("DataSource=:memory:")
                .Options;

            var context = new ApplicationDbContext(options);
            context.Database.OpenConnection();
            context.Database.EnsureCreated();

            return context;
        }
    }
}
