﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversity.Infrastructure.DataAccess.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.UnitTests.Helpers
{
    public static class DbContextExtensions
    {
        public static async Task<ApplicationDbContext> SeedAsync<T>(this DbContext context, IList<T> entities) where T : class 
        {
            if (context == null)
            {
                throw new ArgumentException(nameof(context));
            }

            if (entities == null || !entities.Any())
            {
                throw new ArgumentNullException(nameof(entities));
            }

            await context.Set<T>().AddRangeAsync(entities);
            await context.SaveChangesAsync();

            return context as ApplicationDbContext;
        }
    }
}
