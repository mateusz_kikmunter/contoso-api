﻿using System;
using System.Threading.Tasks;
using ContosoUniversity.Api.ViewModels;
using ContosoUniversity.Core.Abstract;
using ContosoUniversity.Core.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ContosoUniversity.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SudentController : ControllerBase
    {
        private readonly IStudentRepository _repository;

        public SudentController(IStudentRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("{id}", Name = "get-student")]
        public async Task<IActionResult> Get(Guid id)
        {
            var student = await _repository.GetSingleAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "UserMustBeAdmin")]
        public async Task<IActionResult> Create([FromBody] RegisterStudentViewModel viewModel)
        {
            var student = new Student
            {
                Active = viewModel.Active,
                AuthenticationId = viewModel.AuthenticationId,
                Email = viewModel.Email,
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName
            };

            await _repository.AddAsync(student);

            return CreatedAtRoute("get-student", new { id = student.AuthenticationId }, student);
        }
    }
}