﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ContosoUniversity.Api.ViewModels
{
    public class RegisterStudentViewModel
    {
        public Guid AuthenticationId { get; set; }

        [Required]
        [MaxLength(255)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(255)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        public bool Active { get; set; }
    }
}
